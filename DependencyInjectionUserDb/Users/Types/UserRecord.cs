﻿namespace DependencyInjectionUserDb.Users.Types;

public class UserRecord
{
    public Guid Id { get; set; }
    public string Username { get; set; }
    public string Password { get; set; }
    public UserType UserType { get; set; }
    public Dictionary<string, bool> Flags { get; set; }
}