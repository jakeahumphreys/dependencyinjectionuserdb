﻿namespace DependencyInjectionUserDb.Users.Types;

public enum UserType
{
    User = 0,
    Admin = 1
}