﻿using DependencyInjectionUserDb.Common.Errors;

namespace DependencyInjectionUserDb.Users.Types;

public class GetUserResponse
{
    public UserRecord User { get; set; }
    public bool HasError { get; set; }
    public Error Error { get; set; }

    public GetUserResponse WithError(Error error)
    {
        return new GetUserResponse
        {
            HasError = true,
            Error = error
        };
    }
}