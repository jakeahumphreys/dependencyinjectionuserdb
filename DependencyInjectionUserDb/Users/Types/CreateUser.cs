﻿using DependencyInjectionUserDb.Types;
using DependencyInjectionUserDb.Common.Errors;

namespace DependencyInjectionUserDb.Users.Types;

public class CreateUserRequest
{
    public string Username { get; set; }
    public string Password { get; set; }
    public UserType UserType { get; set; }
}

public class CreateUserResponse
{
    public UserRecord User { get; set; }
    public bool HasError { get; set; }
    public Error Error { get; set; }

    public CreateUserResponse WithError(Error error)
    {
        return new CreateUserResponse
        {
            HasError = true,
            Error = error
        };
    }
}