﻿namespace DependencyInjectionUserDb.Types;

public struct UserFlags
{
    public const string IS_USER_BANNED = "IsUserBanned";
}