﻿using DependencyInjectionUserDb.Types;
using DependencyInjectionUserDb.Common.Errors;
using DependencyInjectionUserDb.Users.Types;

namespace DependencyInjectionUserDb.Users;

public interface IUserService
{
    public CreateUserResponse Create(CreateUserRequest request);
    public GetUserResponse GetUser(Guid id);
}

public class UserService : IUserService
{
    private readonly IUserRepository _userRepository;

    public UserService(IUserRepository userRepository)
    {
        _userRepository = userRepository;
    }
    
    public CreateUserResponse Create(CreateUserRequest request)
    {
        var newUser = new UserRecord
        {
            Id = Guid.NewGuid(),
            Username = request.Username,
            Password = request.Password,
            UserType = request.UserType
        };

         _userRepository.Add(newUser);

         var addedUser = _userRepository.Find(newUser.Id);

         if (addedUser == null)
             return new CreateUserResponse().WithError(new Error
             {
                 TechnicalMessage = "User not added to the database.",
                 UserMessage = "An error occurred adding a new user."
             });

         return new CreateUserResponse {User = addedUser};
    }

    public GetUserResponse GetUser(Guid id)
    {
        var user = _userRepository.Find(id);

        if (user == null)
            return new GetUserResponse().WithError(new Error
            {
                TechnicalMessage = "User doesn't exist in the database",
                UserMessage = "User not found."
            });

        return new GetUserResponse
        {
            User = user
        };
    }
}