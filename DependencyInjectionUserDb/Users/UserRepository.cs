﻿using DependencyInjectionUserDb.Users.Types;

namespace DependencyInjectionUserDb.Users;

public interface IUserRepository
{
    public void Add(UserRecord userRecord);
    public UserRecord? Find(Guid id);
}

public class UserRepository : IUserRepository
{
    private List<UserRecord> _localUserStore = new List<UserRecord>();
    
    public void Add(UserRecord userRecord)
    {
        _localUserStore.Add(userRecord);
    }

    public UserRecord? Find(Guid id)
    {
        return _localUserStore.Find(x => x.Id == id);
    }
}