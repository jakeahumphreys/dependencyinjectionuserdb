﻿namespace DependencyInjectionUserDb.Common.Errors;

public class Error
{
    public string UserMessage { get; set; }
    public string TechnicalMessage { get; set; }
}