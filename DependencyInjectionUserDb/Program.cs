﻿using DependencyInjectionUserDb.Users;
using DependencyInjectionUserDb.Users.Types;
using Newtonsoft.Json;

namespace DependencyInjectionUserDb;

public class Program
{
    private readonly IUserService _userService;

    public Program(IUserService userService)
    {
        _userService = userService;
    }

    public void Start()
    {
        Console.WriteLine("Add a new user to the database...");
        
        var userResponse = _userService.Create(new CreateUserRequest
        {
            Username = "Admin",
            Password = "Password",
            UserType = UserType.Admin
        });
        
        if(userResponse.HasError)
            Console.WriteLine(userResponse.Error.UserMessage);
        
        Console.WriteLine(JsonConvert.SerializeObject(userResponse.User));
        
        Console.WriteLine("Try the fetch mechanism...");
        
        var getUserResponse = _userService.GetUser(userResponse.User.Id);
        
        if(getUserResponse.HasError)
            Console.WriteLine(getUserResponse.Error.UserMessage);
        
        Console.WriteLine(JsonConvert.SerializeObject(getUserResponse.User));
    }
}