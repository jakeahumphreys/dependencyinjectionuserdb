﻿using DependencyInjectionUserDb.Users;
using Microsoft.Extensions.DependencyInjection;

namespace DependencyInjectionUserDb;

public class Startup
{
    public static void Main(string[] args)
    {
        var services = new ServiceCollection();
        ConfigureServices(services);
        services.AddSingleton<Program, Program>()
            .BuildServiceProvider()
            .GetService<Program>()
            ?.Start();
    }

    public static void ConfigureServices(IServiceCollection services)
    {
        services
            .AddSingleton<IUserService, UserService>()
            .AddSingleton<IUserRepository, UserRepository>();
    }
}